
# Treehouse - Ruby Operators and Control Structures #

This repository contains notes and practice examples from **Ruby Operators and Control Structures**, imparted by Jason Seifer at [Treehouse](https://teamtreehouse.com).

> In this course, you'll learn about operators, which let you assign values to variables and test the value inside those variables. We'll also explore control structures which let us control how our program runs, making our programs more efficient and intelligent.

## Contents ##

- **Ruby Operators**: Ruby Operators are used for two different purposes: assignment and comparison. In this stage, we'll learn how to use the different operators in Ruby to assign and compare variables.
- **Ruby Control Structures**: Control structures allow programs to take different paths depending on different values and conditions. We'll build on our new knowledge of operators to work through branching our programs.
 - **Logical Operators**: Logical operators allow us to evaluate more than one expression at a time. This gives us immense power when writing programs.

---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.
