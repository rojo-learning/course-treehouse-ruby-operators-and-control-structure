
# Comparison Operators
## Equality Operators
a, b = 10, 11

### Equality
a == a # => true
a == b # => false

### Inequality
a != a # => false
a != b # => true

## Greater & Less Than
### Greater Than
a > a # => false
a > b # => false
b > a # => true
"a" > "b" # => false ## "a".ord => 97; "b".ord => 98
"b" > "b" # => false
"a" > "A" # => true  ## "a".ord => 97; "A".ord => 65

### Less Than
a < a # => false
a < b # => true
b < a # => false
"a" < "b" # => true ## "a".ord => 97; "b".ord => 98
"b" < "b" # => false
"B" < "b" # => true  ## "b".ord => 98; "B".ord => 66

## Greater and Less Than or Equal To

### Greater Than or Equal
a >= a # => true
a >= b # => false
b >= a # => true
"a" >= "b" # => false ## "a".ord => 97; "b".ord => 98
"b" >= "b" # => true
"a" >= "A" # => true  ## "a".ord => 97; "A".ord => 65

### Less Than or Equal
a <= a # => true
a <= b # => true
b <= a # => false
"a" <= "b" # => true ## "a".ord => 97; "b".ord => 98
"b" <= "b" # => true
"B" <= "b" # => true  ## "b".ord => 98; "B".ord => 66
