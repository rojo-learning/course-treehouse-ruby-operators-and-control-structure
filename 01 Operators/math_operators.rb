
# Math Operators
## Variable declaration and assignment
year = 3000

## Common math operators
### Addition
year + 1 # => 3001

### Subtraction
year - 1 # => 2999

### Multiplication
year * 2 # => 6000

### Division
year / 2 # => 1500

### Remainder
year % 2 # => 0

### Exponentiation
year ** 2 # => 9_000_000

## Operation and assignment at the same time
year +=  1 # => 3001
year -=  1 # => 3000
year *=  2 # => 6000
year /=  3 # => 1000
year %=  3 # => 1
year **= 2 # => 1

## Math operators with strings
### String concatenation
"Hello" + " " + "World" # => "Hello World"

### String duplication and concatenation
"Hello " * 2 # => "Hello Hello "
